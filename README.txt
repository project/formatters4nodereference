The Formatters for Node Reference field module adds the ability to use any
non-Node Reference formatter to format a single field from a Node via the Node
Reference field, using either the first available field of the required type, or
by using the chosen field set via the Formatter Settings.

An example of this would be to render a Node Reference as the referenced Nodes
ImageField using an ImageCache preset.

Formatters for Node Reference field was written and is maintained
by Stuart Clark (deciphered).
- http://stuar.tc/lark


Required modules
--------------------------------------------------------------------------------

* Formatter Settings module.
