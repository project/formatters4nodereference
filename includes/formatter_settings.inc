<?php
/**
 * @file
 * Formatter Settings module integration.
 */

/**
 * Implements hook_formatter_settings_form().
 */
function formatters4nodereference_formatter_settings_form($content_type, $field_name, $formatter_name, $settings) {
  $element = array();

  list($module, $field_type, $formatter_name) = explode('__', $formatter_name);
  $field = content_fields($field_name);
  foreach (array_filter($field['referenceable_types']) as $type) {
    $type = content_types($type);
    if ($type['fields']) {
      $options = array();
      foreach ($type['fields'] as $field) {
        if ($field['type'] == $field_type) {
          $options[$field['field_name']] = $field['widget']['label'];
        }
      }

      if ($options) {
        $element["{$type['type']}"] = array(
          '#type' => 'select',
          '#title' => t('!type field', array('!type' => $type['name'])),
          '#options' => $options,
          '#default_value' => isset($settings[$type['type']]) ? $settings[$type['type']] : current(array_keys($options)),
        );
      }
    }
  }

  return $element;
}

/**
 * Implements hook_formatter_settings_summary().
 */
function formatters4nodereference_formatter_settings_summary($content_type, $field_name, $formatter_name, $settings) {
  $summary = '';

  list($module, $field_type, $formatter_name) = explode('__', $formatter_name);
  $field = content_fields($field_name);
  foreach (array_filter($field['referenceable_types']) as $type) {
    $type = content_types($type);
    if ($type['fields']) {
      foreach ($type['fields'] as $field) {
        if ($field['type'] == $field_type) {
          $summary .= t('!type field', array('!type' => $type['name'])) . ': ' . (isset($settings[$type['type']]) ? $settings[$type['type']] : '<em>' . t('Default') . '</em>') .  '<br />';
          break;
        }
      }
    }
  }

  return $summary;
}
